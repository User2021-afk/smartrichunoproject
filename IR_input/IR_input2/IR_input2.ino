//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Программа ифрокрасного приёмника, включающего управление  с пульта дисплэем
//1.0V
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "IRremote.h"
#include "RichUNOTM1637.h"
#include <Temperature_LM75_Derived.h>
Generic_LM75 temperature;
IRrecv irrecv(2); // указываем вывод, к которому подключен приемник
decode_results results;
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);


void setup() {
  Serial.begin(9600); // выставляем скорость COM порта
  disp.init();
  Wire.begin();
  irrecv.enableIRIn(); // запускаем прием
  pinMode(9, OUTPUT);
}

void loop() {
  if (irrecv.decode(&results)) { // если данные пришли
    irrecv.resume(); // принимаем следующую команду
  }
  Serial.println(results.value, HEX); // печатаем данные
  if (results.value == 0x10) {
    Serial.print(1); 
    disp.display(1);
    delay(1000);
    }
  
  if (results.value == 0x810) {
    Serial.print(2); 
    disp.display(2);
    delay(1000);
  }
  if (results.value == 0x410) {
    Serial.print(3); 
    disp.display(3);
    delay(1000);
  }
  if (results.value == 0xC10) {
    Serial.print(4); 
    disp.display(4);
    delay(1000);
  }
  if (results.value == 0x210) {
    Serial.print(5); 
    disp.display(5);
    delay(1000);
  }
  if (results.value == 0xA10) {
    Serial.print("Temperature = ");
    disp.display(temperature.readTemperatureC());
    Serial.println(" C ");
    delay(250);
   }
   if (results.value == 0x610) {
   digitalWrite(9, HIGH);
   delay(100);
   digitalWrite(9, LOW);
   delay(1000);
   }
   
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
