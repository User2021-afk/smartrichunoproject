//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
//Программа ифрокрасного приёмника, включающего с пульта 13-й светодиод и управление дисплэем
//1.0V
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "IRremote.h"
#include "RichUNOTM1637.h"
IRrecv irrecv(2); // указываем вывод, к которому подключен приемник
decode_results results;
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);


void setup() {
  Serial.begin(9600); // выставляем скорость COM порта
  disp.init();
  irrecv.enableIRIn(); // запускаем прием
  pinMode(13, OUTPUT);
}

void loop() {
  if (irrecv.decode(&results)) { // если данные пришли
    irrecv.resume(); // принимаем следующую команду
  }
  Serial.println(results.value, HEX); // печатаем данные
  //if FF6897
  if (results.value == 0xFF6897) {
    digitalWrite(13, HIGH);
    delay(1000);
  }
  else {
    digitalWrite(13, LOW);
    delay(1000);
  }
  if (results.value == 0xFF9867) {
    Serial.print(1000); 
    disp.display(1000);
    delay(1000);
  }
  if (results.value == 0xFFB04F) {
    Serial.print(2000); 
    disp.display(2000);
    delay(1000);
  }
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
