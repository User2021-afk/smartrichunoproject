
//-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// Программа управления сенсерными кнопками
// V1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
#include "RichUNOTM1637.h"
#include <LiquidCrystal_I2C.h> // Библиотека I2C дисплея
//#include "LiquidCrystal_I2C_Menu_Btns.h"
LiquidCrystal_I2C lcd(0x27, 16, 2);
const int CLK = 10;
const int DIO = 11;
TM1637 disp(CLK, DIO);

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  lcd.backlight();
    lcd.init(); // Инициализация LCD
}

void loop() {
  const int TOUCH1 = 3;
  const int TOUCH2 = 4;
  const int TOUCH3 = 5;
  const int TOUCH4 = 6;
 
  if (digitalRead(TOUCH1) == true) {
   lcd.setCursor(0, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Hello World!   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Voice:     ");
    Serial.print("Touch 1 ");
    Serial.print(digitalRead(TOUCH1));
}
  if (digitalRead(TOUCH2) == true) {
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Motor:     ");
    Serial.print("Touch 2 ");
    Serial.print(digitalRead(TOUCH2));
}  
  if (digitalRead(TOUCH3) == true) {
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Voice:     ");
    Serial.print("Touch 3 ");
    Serial.print(digitalRead(TOUCH3));
}  
  if (digitalRead(TOUCH4) == true) {
    lcd.setCursor(2, 0);      // курсор на 4-й символ 1-й строки
    lcd.print("  Choose menu   "); // Тест на 1-й строке экрана
    lcd.setCursor(0, 1);
    lcd.print("Menu Sonar:     ");
    Serial.print("Touch 4 ");
    Serial.println(digitalRead(TOUCH4));
  } 
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
// END FILI
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=//
